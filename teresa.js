ip=window.location.hostname;
if (ip=='')
    ip='127.0.0.1';

// to connect from a remote client, set modim IP here
// ip='10.0.1.200'

console.log("Trying connections...")

codeport = 9010;
codeurl = "ws://"+ip+":"+codeport+"/websocketserver";
console.log(codeurl);
codews = new WebSocket(codeurl);

codews.onopen = function(){
  console.log("codews connection received");
  codews.send('$$teresa'); // name of the folder containing the demo
}

ctrlport = 9110;
ctrlurl = "ws://"+ip+":"+ctrlport+"/ctrlwebsocketserver";
console.log(ctrlurl);
ctrlws = new WebSocket(ctrlurl);

ctrlws.onopen = function(){
  console.log("ctrlws connection received");
  document.getElementById("status").innerHTML = "<font color='green'>OK</font>";
} 

ctrlws.onclose = function(){
  console.log("ctrlws connection closed");
  document.getElementById("status").innerHTML = "<font color='red'>NOT CONNECTED</font>";
}

function ctrlbtn(event) {
    console.log('control '+event.id);
    ctrlws.send(event.id);
}

function codebtn(event) {
    console.log('code '+event.id);
    codews.send('$'+event.id);
}

function initbtn() {
    console.log("init");
    codews.send("robot.normalPosture()\nrobot.setAlive(True)\nrobot.setLanguage('it')\nim.setProfile(['*', '*', 'it', '*'])\nim.display.loadUrl('index.html')\nrobot.startFaceDetection()\n");
}

function saybtn(event) {
    var text = document.getElementById("ta"+event.id).value;
    console.log("say "+text);
    codews.send("robot.say('"+text+"')\nrobot.normalPosture()");
}

function cmdbtn(event) {
    console.log("cmd "+event.id);
    codews.send(event.id);
}

function actionbtn(event) {
    console.log("action "+event.id);
    codews.send("im.execute('"+event.id+"')\nrobot.normalPosture()");
}

function raiseArm(which) {
    code = "robot.setAlive(False)\n";
    code += "robot.raiseArm('"+which+"')\n";
    codews.send(code);
}

function quizbtn(event) {
    code = "im.askUntilCorrect('"+event.id+"',600)\n"; // no timeout = 600 s
    codews.send(code);
}
    
function quiz2btn(event) {
    code = "im.askUntilCorrect('"+event.id+"',600)\nim.execute('"+event.id+"b')\n"; // timeout = 600 s
    codews.send(code);
}


function look(where) {
    code = "im.robot.headPose";
    if (where=='left')
        code += "(1.0, -0.2, 3.0)";
    else if (where=='right')
        code += "(-1.0, -0.2, 3.0)";
    else 
        code += "(0.0, -0.2, 3.0)";
    codews.send(code);
}




