# ciao demo

import os
import sys
import time
import random

try:
    sys.path.insert(0, os.getenv('MODIM_HOME')+'/src/GUI')
except Exception as e:
    print "Please set MODIM_HOME environment variable to MODIM folder."
    sys.exit(1)

import ws_client
from ws_client import *


def presentazione():

    begin()

    im.setProfile(['*', '*', 'it', '*'])
    im.display.loadUrl('fullslides.html')

    im.robot.setAlive(True)

    im.execute('stilevita19_1', audio=False)

    im.robot.normalPosture()

    time.sleep(1)
    im.execute('stilevita19_1')
    time.sleep(1)
    im.execute('stilevita19_2')
    time.sleep(1)

    im.execute('stilevita19_1', audio=False)

    im.robot.normalPosture()
 
    end()


def concorso():

    begin()
    im.execute('concorso1', audio=False)
    im.robot.normalPosture()
    end()



if __name__ == "__main__":

    mws = ModimWSClient()

    # local execution
    #mws.setDemoPathAuto(__file__)

    # remote execution
    # Set MODIM_IP to connnect to remote MODIM server
    # Use absolute path of the demo folder
    mws.setDemoPath('/home/nao/teresa')

    mws.run_interaction(concorso)


