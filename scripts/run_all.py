#!/usr/bin/env python

import os
import time

def send_command(cmd, name):
    print cmd

    use_tmux = os.system('tmux -V')==0
    
    if use_tmux:
        sessionname = 'demo'
        r = os.system('tmux select-window -t %s:0' %sessionname)
        if r!=0:
            os.system('tmux -2 new-session -d -s %s' %sessionname)
        r = os.system('tmux select-window -t %s' %name)
        if (r!=0): # if not existing
            os.system('tmux new-window -n %s' %name)
        os.system('tmux send-keys "%s" C-m' %cmd)
    else:
        os.system('xterm -hold -e "%s" &' %cmd)


def main():
    #send_command('python -m SimpleHTTPServer 8000','http')
    cwd = os.getcwd()
    cwd += '/..'
    send_command('cd %s' %cwd,'modim')
    use_pepper = os.getenv('PEPPER_IP')=='127.0.0.1' or os.getenv('PEPPER_IP')=='localhost'
    if use_pepper:
        # Pepper robot
        send_command('PYTHONPATH=$PYTHONPATH:$MODIM_HOME/src/GUI:$MODIM_HOME/src/action python -m ws_server -robot pepper','modim')
    else:
        # Linux machine
        send_command('PYTHONPATH=$PYTHONPATH:$MODIM_HOME/src/GUI:$MODIM_HOME/src/action python -m ws_server','modim')
    time.sleep(2)
    send_command('cd %s' %cwd,'browser')
    if use_pepper:
        # Pepper robot
        send_command('cd ~/pepper_tools/tablet','browser')
        send_command('python show_web.py --url teresa/index.html','browser')
    else:
        # Linux machine
        send_command('firefox index.html &','browser')
        send_command('firefox m.html &','browser')
    time.sleep(2)
    #send_command('python dn.py','py')



if __name__ == '__main__':
    main()

