# TERESA demo

#export PYTHONPATH=$PYTHONPATH:$MODIM_HOME/src/GUI:$MODIM_HOME/src/action
#python -m ws_server -robot pepper

import os
import sys
import time
#import joystick
import random

if (os.getenv("MODIM_HOME")==None):
    print("Plaese set MODIM_HOME environment variable")
    sys.exit(0)


modimdir = os.getenv('MODIM_HOME')+'/src/GUI'

sys.path.append(modimdir)

from ws_client import *
import ws_client


#event_map = { 'BLUE':'surv' ,'GREEN':'welcome', 'RED':'welcomeprofumo', 'ORANGE':'quest', 'LB':'','RB':'','LT':'','RT':'','BACK':'back','START':'home'}

#def joystick_cb():
#    global event_map
#    print joystick.lastevent()
#    csend_noblock('*'+event_map[joystick.lastevent()])

#joystick.start(joystick_cb)


def presentazione():

    begin()

    im.robot.normalPosture()

    time.sleep(1)
    im.execute('ciao2')
    time.sleep(2)
    im.execute('ciao3')
    time.sleep(2)
    im.execute('stilevita')
    time.sleep(2)
    im.execute('tavolarotonda')
    time.sleep(3)
    im.execute('ciao4')
    time.sleep(3)
    im.execute('ciao1')

    im.robot.normalPosture()
 
    end()


def intro():
    begin()

    im.setProfile(['*', '*', 'it', '*'])
    im.display.loadUrl('fullslides.html')

    im.robot.setAlive(True)

    im.execute('ciao1')

    end()


def quiz():

    im.robot.normalPosture()

    a = ''
    while a!='correct':
        a = im.ask('carboidrati')
        im.execute(a)
        time.sleep(5)

    im.robot.normalPosture()

    a = ''
    while a!='correct':
        a = im.ask('aerobica')
        im.execute(a)
        time.sleep(5)

    im.robot.normalPosture()

    a = ''
    while a!='correct':
        a = im.ask('anaerobica')
        im.execute(a)
        time.sleep(5)

    im.robot.normalPosture()


def ciao():
    begin()
    im.setProfile(['*', '*', 'it', '*'])
    im.display.loadUrl('fullslides.html')
    im.robot.normalPosture()
    im.execute('ciao1')    
    im.robot.normalPosture()
    end()

def stilevita():

    begin()
    im.setProfile(['*', '*', 'it', '*'])
    im.display.loadUrl('stilevita.html')
    im.robot.normalPosture()
    im.robot.setAlive(True)

    a = ''
    while a!='exit' and a!='home':
        a = im.display.answer()

        if a=='reset':
            im.display.loadUrl('stilevita.html')

        if a=='1':
            im.executeModality('image_1', 'img/alimentazione.png')
            im.execute('stilevita_ali')
        elif a=='2':
            im.executeModality('image_2', 'img/attivitafisica.png')
            im.execute('stilevita_att')
        elif a=='3':
            im.executeModality('image_3', 'img/valori.png')
            im.execute('stilevita_val')
        elif a=='4':
            im.executeModality('image_4', 'img/relazioni.png')
            im.execute('stilevita_rel')
        elif a=='5':
            im.executeModality('image_5', 'img/tempolibero.png')
            im.execute('stilevita_tem')
        elif a=='6':
            im.executeModality('image_6', 'img/pensieri.png')
            im.execute('stilevita_pen')
        elif a=='7':
            im.executeModality('image_7', 'img/sonno.png')
            im.execute('stilevita_son')
        elif a=='8':
            im.executeModality('image_8', 'img/abitudini.png')
            im.execute('stilevita_abi')

        if a=='?':
            im.execute('stilevita_qualicomp')

        im.robot.normalPosture()


    if (a=='home'):
        im.display.loadUrl('index.html')
    else:
        im.display.loadUrl('black.html')
    
    end()





def emozioni():

    begin()
    im.setProfile(['*', '*', 'it', '*'])
    im.display.loadUrl('emozioni.html')
    im.robot.normalPosture()
    im.robot.setAlive(True)

    a = ''
    while a!='exit' and a!='home':
        a = im.display.answer()

        if a=='reset':
            im.display.loadUrl('emozioni.html')

        if a=='1':
            im.executeModality('image_1', 'img/paura.png')
            im.execute('emozioni_1')
        elif a=='2':
            im.executeModality('image_2', 'img/rabbia.png')
            im.execute('emozioni_2')
        elif a=='3':
            im.executeModality('image_3', 'img/tristezza.png')
            im.execute('emozioni_3')
        elif a=='4':
            im.executeModality('image_4', 'img/gioia.png')
            im.execute('emozioni_4')
        elif a=='5':
            im.executeModality('image_5', 'img/disgusto.png')
            im.execute('emozioni_5')
        elif a=='6':
            im.executeModality('image_6', 'img/sorpresa.png')
            im.execute('emozioni_6')

        #if a=='?':
        #    im.execute('stilevita_qualicomp')

        im.robot.normalPosture()


    if (a=='home'):
        im.display.loadUrl('index.html')
    else:
        im.display.loadUrl('black.html')

    #im.robot.stop_face_recording()
    
    end()




def sax():

    im.robot.sax()


if __name__ == "__main__":

    mws = ModimWSClient()
    # local execution
    qmws.setDemoPathAuto(__file__)

    # remote execution
    # Set MODIM_IP to connnect to remote MODIM server
    # Use absolute path of the demo folder
    #mws.setDemoPath('/home/nao/teresa')


    #mws.store_interaction(presentazione)
    #mws.store_interaction(quiz)

    #mws.run_interaction(intro)
    #time.sleep(3)
    #mws.run_interaction(presentazione)
    #mws.run_interaction(quiz)

    mws.run_interaction(ciao)


